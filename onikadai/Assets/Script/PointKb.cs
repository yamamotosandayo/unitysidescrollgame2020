﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointKb : MonoBehaviour
{
    public int point;   //これが得点

    GameObject Score;   //これはテキストを入れる箱

    // Start is called before the first frame update
    void Start()
    {
        Score = GameObject.Find("Score");   //箱にテキストを見つけてきて入れる
    }

    // Update is called once per frame
    void Update()
    {
        //スコアの中のテキストの中のテキストをこう変えろって文
        //score:って文章とpointって変数を合わせて表示させている
        Score.GetComponent<Text>().text = "score:" + point;
    }
}
