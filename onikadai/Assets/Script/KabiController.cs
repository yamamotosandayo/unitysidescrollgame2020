﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KabiController : MonoBehaviour
{
    GameObject player;  //usaを入れる箱
    GameObject score;   //テキストを入れる箱
    public float kabisokudo;    //カビが動く速さ(宣言しかしてないからunity上で変更する)

    // Start is called before the first frame update
    void Start()
    {

        player = GameObject.Find("baikinnmann");    //この箱にusaぶっこめ

        score = GameObject.Find("Score");   //この箱にscoreぶっこめ


    }

    // Update is called once per frame
    void Update()
    {
        //この分動かす(xは変数)
        transform.Translate(kabisokudo, 0, 0); //フレームごとに等速で落下させるんだよ

        if (transform.position.x < -10.0f) //画面の外に出たら
        {
            Destroy(gameObject);//オブジェクトが消えるようにする
        }

        Vector2 p1 = transform.position; //当たり判定 p1はカビの中心座標です　
        Vector2 p2 = player.transform.position;　//p2はusaの中心座標です
        Vector2 dir = p1 - p2; //usaから見たカビの位置
        float d = dir.magnitude;
        float r1 = 0.5f; //カビの当たり判定の半径
        float r2 = 1.0f; //usa(usa)の当たり判定の半径

        //もし衝突していたら(もし半径を足した距離より近かったら)
        if (d < r1 + r2)
        {
            //テキストの中のポイントをプラス3000する
            score.GetComponent<PointKb>().point += 3000;
            Destroy(gameObject);//衝突した場合は矢を消す
        }
    }
}
