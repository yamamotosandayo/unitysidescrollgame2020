﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class KabaoController : MonoBehaviour
{
    GameObject player;

    public float kabaosokudo;

    public int goriraHP = 10;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("baikinnmann");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(kabaosokudo, 0, 0); //フレームごとに等速で落下させるんだよ

        if (transform.position.x < -10.0f) //画面の外に出たら
        {
            Destroy(gameObject);//オブジェクトが消えるようにする
        }

        Vector2 p1 = transform.position; //当たり判定 p1は矢の中心座標です　
        Vector2 p2 = player.transform.position; 　//p2はbaikinnmannの中心座標です
        Vector2 dir = p1 - p2; //usaから見た矢の位置
        float d = dir.magnitude;
        float r1 = 0.5f; //矢の当たり判定の半径
        float r2 = 1.0f; //usa(usa)の当たり判定の半径

        if (d < r1 + r2)
        {
            GameObject director = GameObject.Find("GameDirector");
            director.GetComponent<GameDirector>().DecreaseHp();
            Destroy(gameObject);//衝突した場合は矢を消す
        }

        
           
     }
}
