﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowGenerator : MonoBehaviour
{
    public GameObject arrowPrefab;
    public float span = 3.0f;
    float delta = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        delta += Time.deltaTime;
        //Debug.Log(delta);
        if (delta > span)
        {
            delta = 0;


            GameObject go = Instantiate(arrowPrefab);
            int py = Random.Range(-5, 5); //←これは-5から4になる5にはならない
            go.transform.position = new Vector3(8, py, 0);
        }
    }
}
